from rest_framework import serializers
from .models import Result, RunUsertaskResult, UserTask
from django.contrib.auth.models import User

class UserTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTask
        fields = '__all__'

class ResultsSerializer(serializers.Serializer):
        result_id = serializers.IntegerField()
        url_lot = serializers.CharField(max_length=10000)
        lot_number = serializers.CharField(max_length=100)
        pre_lot_text = serializers.CharField(max_length=100000)
        lot_title = serializers.CharField(max_length=50000)
        artist_creator_brand = serializers.CharField(max_length=1000)
        price = serializers.IntegerField()
        estimate_low = serializers.IntegerField()
        estimate_high = serializers.IntegerField()
        currency = serializers.CharField(max_length=1000)
        status = serializers.CharField(max_length=1000)
        date_of_sale = serializers.DateTimeField()
        lot_description = serializers.CharField(max_length=100000)
        provenance = serializers.CharField(max_length=20000)
        literature = serializers.CharField(max_length=20000)
        lot_essay = serializers.CharField(max_length=200000)
        special_notice = serializers.CharField(max_length=100000)
        post_lot_text = serializers.CharField(max_length=100000)
        sale_room_notice = serializers.CharField(max_length=20000)
        condition_report = serializers.CharField(max_length=20000)
        has_video = serializers.BooleanField()
        date_of_manufacture = serializers.CharField(max_length=2000)
        period = serializers.CharField(max_length=2000)
        design = serializers.CharField(max_length=2000)
        style = serializers.CharField(max_length=2000)
        creation_type = serializers.CharField(max_length=2000)
        sub_type = serializers.CharField(max_length=2000)
        lot_category = serializers.CharField(max_length=2000)
        serial_number = serializers.CharField(max_length=2000)
        materials = serializers.CharField(max_length=2000)
        gemstone = serializers.CharField(max_length=2000)
        stone_cut = serializers.CharField(max_length=2000)
        stone_count = serializers.CharField(max_length=2000)
        carat_total_weight = serializers.CharField(max_length=2000)
        total_item_weight = serializers.CharField(max_length=2000)
        weight_unit = serializers.CharField(max_length=2000)
        dimensions = serializers.CharField(max_length=2000)
        image = serializers.JSONField()
        is_not_accessible = serializers.BooleanField()
        is_vca = serializers.BooleanField()
        cluster_hash = serializers.CharField(max_length=200)
        cluster_id = serializers.IntegerField()
        filling_details = serializers.JSONField()
        user_task_id = serializers.IntegerField()
        url_sale = serializers.CharField(max_length=10000)
        scraping_time = serializers.DateTimeField()
        module_id = serializers.IntegerField()
        created_at = serializers.DateTimeField()
        is_active = serializers.BooleanField()
        params = serializers.JSONField()
        source = serializers.CharField(max_length=1000)
        source_type = serializers.CharField(max_length=1000)
        name = serializers.CharField(max_length=3000)
        starting_date = serializers.DateTimeField()
        ending_date = serializers.DateTimeField()
        timezone = serializers.CharField(max_length=100)
        sale_number = serializers.CharField(max_length=1000)
        is_online = serializers.BooleanField()
        location = serializers.CharField(max_length=1000)
        sale_status = serializers.CharField(max_length=1000)
        sale_total = serializers.IntegerField()
        sale_total_currency = serializers.CharField(max_length=1000)
        total_results = serializers.IntegerField()
        is_keyword_in_title = serializers.BooleanField()
        department = serializers.CharField(max_length=1000)
        module_params = serializers.JSONField()
        hash_value = serializers.CharField(max_length=32)


class RunUsertaskResultSerializer(serializers.ModelSerializer):
    result = ResultsSerializer()
    user_task = UserTaskSerializer()
    class Meta:
        model = RunUsertaskResult
        fields = '__all__'

    def to_representation(self, instance):
        data = super(RunUsertaskResultSerializer,
                     self).to_representation(instance)
        
        result = data.pop('result')
        for key, val in result.items():
            if key == 'id':
                key = 'result_id'
            if key == 'url':
                key = 'url_lot'
            data.update({key: val})
        user_task = data.pop('user_task')
        for key, val in user_task.items():
            if key == 'id':
                key = 'user_task_id'
            if key == 'url':
                key = 'url_sale'
            data.update({key: val})
        return data



class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password')

