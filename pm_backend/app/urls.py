from django.urls import path, include, re_path
from rest_framework import routers
from.views import *

urlpatterns = [
    path("register", RegisterView.as_view(), name="register"),
    path("login", LoginView.as_view(), name="login"),
    re_path('jewelry-list$', JewelryListView.as_view(), name='jewelry list'),
    re_path('jewelry-list/(?P<jewelry_id>\d+)$', JewelryListView.as_view(), name='jewelry  detail')
]
