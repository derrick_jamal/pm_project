# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import User

User._meta.get_field('email')._unique = True
User.USERNAME_FIELD = 'email'
User._meta.get_field('username')._unique = False
User.REQUIRED_FIELDS = ['password']



class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AuthtokenToken(models.Model):
    key = models.CharField(primary_key=True, max_length=40)
    created = models.DateTimeField()
    user = models.OneToOneField(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'authtoken_token'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Module(models.Model):
    name = models.CharField(unique=True, max_length=100, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)
    backend = models.CharField(max_length=30, blank=True, null=True)
    database = models.CharField(max_length=30, blank=True, null=True)
    alchemy_directory = models.CharField(max_length=30, blank=True, null=True)
    is_public = models.BooleanField(blank=True, null=True)
    is_premium = models.BooleanField(blank=True, null=True)
    is_available = models.BooleanField(blank=True, null=True)
    repository = models.CharField(max_length=50, blank=True, null=True)
    public_name = models.CharField(max_length=200, blank=True, null=True)
    hash_value = models.CharField(unique=True, max_length=100, blank=True, null=True)
    rank = models.IntegerField(blank=True, null=True)
    fee_multiplier = models.IntegerField(blank=True, null=True)
    labels = models.JSONField(blank=True, null=True)
    extension = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'module'


class Result(models.Model):
    url = models.CharField(unique=True, max_length=2000, blank=True, null=True)
    lot_number = models.CharField(max_length=100, blank=True, null=True)
    pre_lot_text = models.CharField(max_length=100000, blank=True, null=True)
    lot_title = models.CharField(max_length=50000, blank=True, null=True)
    artist_creator_brand = models.CharField(max_length=1000, blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    estimate_low = models.IntegerField(blank=True, null=True)
    estimate_high = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=1000, blank=True, null=True)
    status = models.CharField(max_length=1000, blank=True, null=True)
    date_of_sale = models.DateTimeField(blank=True, null=True)
    lot_description = models.CharField(max_length=100000, blank=True, null=True)
    provenance = models.CharField(max_length=20000, blank=True, null=True)
    literature = models.CharField(max_length=20000, blank=True, null=True)
    lot_essay = models.CharField(max_length=200000, blank=True, null=True)
    special_notice = models.CharField(max_length=100000, blank=True, null=True)
    post_lot_text = models.CharField(max_length=100000, blank=True, null=True)
    sale_room_notice = models.CharField(max_length=20000, blank=True, null=True)
    condition_report = models.CharField(max_length=20000, blank=True, null=True)
    has_video = models.BooleanField(blank=True, null=True)
    date_of_manufacture = models.CharField(max_length=2000, blank=True, null=True)
    period = models.CharField(max_length=2000, blank=True, null=True)
    design = models.CharField(max_length=2000, blank=True, null=True)
    style = models.CharField(max_length=2000, blank=True, null=True)
    creation_type = models.CharField(max_length=2000, blank=True, null=True)
    sub_type = models.CharField(max_length=2000, blank=True, null=True)
    lot_category = models.CharField(max_length=2000, blank=True, null=True)
    serial_number = models.CharField(max_length=2000, blank=True, null=True)
    materials = models.CharField(max_length=2000, blank=True, null=True)
    gemstone = models.CharField(max_length=2000, blank=True, null=True)
    stone_cut = models.CharField(max_length=2000, blank=True, null=True)
    stone_count = models.CharField(max_length=2000, blank=True, null=True)
    carat_total_weight = models.CharField(max_length=2000, blank=True, null=True)
    total_item_weight = models.CharField(max_length=2000, blank=True, null=True)
    weight_unit = models.CharField(max_length=2000, blank=True, null=True)
    dimensions = models.CharField(max_length=2000, blank=True, null=True)
    image = models.JSONField(blank=True, null=True)
    is_not_accessible = models.BooleanField(blank=True, null=True)
    is_vca = models.BooleanField(blank=True, null=True)
    cluster_hash = models.CharField(max_length=200)
    filling_details = models.JSONField(blank=True, null=True)
    scraping_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'result'


class RunUsertaskResult(models.Model):
    matrix_cluster_id = models.IntegerField(blank=True, null=True)
    matrix_cluster_hash = models.CharField(max_length=32, blank=True, null=True)
    matrix_run_id = models.IntegerField(blank=True, null=True)
    matrix_run_hash = models.CharField(max_length=32, blank=True, null=True)
    matrix_user_task_hash = models.CharField(max_length=32, blank=True, null=True)
    matrix_user_task_id = models.CharField(max_length=32, blank=True, null=True)
    result = models.ForeignKey(Result, models.DO_NOTHING)
    user_task = models.ForeignKey('UserTask', models.DO_NOTHING)
    association_time = models.DateTimeField(blank=True, null=True)
    last_scraping_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'run_usertask_result'


class UserTask(models.Model):
    scraping_time = models.DateTimeField(blank=True, null=True)
    module_id = models.IntegerField()
    created_at = models.DateTimeField()
    is_active = models.BooleanField(blank=True, null=True)
    params = models.JSONField()
    url = models.CharField(max_length=10000, blank=True, null=True)
    cluster_hash = models.CharField(max_length=200)
    cluster_id = models.IntegerField()
    source = models.CharField(max_length=1000, blank=True, null=True)
    source_type = models.CharField(max_length=1000, blank=True, null=True)
    name = models.CharField(max_length=3000, blank=True, null=True)
    starting_date = models.DateTimeField(blank=True, null=True)
    ending_date = models.DateTimeField(blank=True, null=True)
    timezone = models.CharField(max_length=100, blank=True, null=True)
    sale_number = models.CharField(max_length=1000, blank=True, null=True)
    is_online = models.BooleanField(blank=True, null=True)
    location = models.CharField(max_length=1000, blank=True, null=True)
    sale_status = models.CharField(max_length=1000, blank=True, null=True)
    sale_total = models.IntegerField(blank=True, null=True)
    sale_total_currency = models.CharField(max_length=1000, blank=True, null=True)
    total_results = models.IntegerField(blank=True, null=True)
    is_keyword_in_title = models.BooleanField(blank=True, null=True)
    department = models.CharField(max_length=1000, blank=True, null=True)
    filling_details = models.JSONField(blank=True, null=True)
    module_params = models.JSONField()
    hash_value = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_task'
        unique_together = (('id', 'module_id'),)
