from django.shortcuts import render
from django.db.models import Q, F
from django.http import HttpResponse,JsonResponse
from django.contrib.auth.hashers import check_password

from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import APIException

from django.contrib.auth.models import User
from django.forms.models import model_to_dict

from .serializers import *
from .models import *

from collections import OrderedDict
import csv
import pandas as pd
from io import StringIO

import re

from datetime import datetime
import json

class APIException(APIException):
    
    def __init__(self, name, detail, code=400):
        self.detail = detail
        self.status_code = code
        self.__class__.__name__ = name



class LargeResultsSetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'limit'
    max_page_size = 2000

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('limit', self.page.paginator.per_page),
            ('page', self.page.number),
            ('total_pages', self.page.paginator.num_pages),
            ('result_from', ((self.page.number - 1) * self.page.paginator.per_page) + 1),
            ('result_to', ((self.page.number - 1) * self.page.paginator.per_page + len(data)) + 1),
            ('data', data),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
        ]))

class RegisterView(APIView):
    def post(self, request):
        email = self.request.data.get('email')
        password = self.request.data.get('password')
        is_staff = self.request.data.get('is_staff', None)
        is_superuser = self.request.data.get('is_superuser', None)

        params = ['email', 'password']
        keys = self.request.data.keys()
        for param in params:
            if param not in keys:
                raise CustomAPIException("ParamsNeeded", f"{param}", code=400)

        if not User.objects.filter(email=email).exists():
            user = User.objects.create_user(username="default", email=email)
            user.set_password(password)
            user.is_active = True
            user.is_staff = True if is_staff and is_staff.lower() != 'false' else False
            user_is_superuser = True if is_superuser and is_superuser.lower() != 'false' else False

            user.save()
            Token.objects.create(user=user)
            return JsonResponse(model_to_dict(user))
        else:
            raise APIException('FormValidator', 'email already exists', code=400)


class LoginView(CreateAPIView):
    serializer_class = LoginSerializer

    def post(self, request, ):
        email = request.data.get("email")
        password = request.data.get("password")
        user = User.objects.filter(email=email).first()
        if not user :
            return Response({"error": "Wrong User Credentials"})
        if user:
            pwd_valid = check_password(password, user.password)
            if pwd_valid:
                return Response({"token": user.auth_token.key})
        return Response({"error": "Wrong Credentials"})




class JewelryListView(APIView):
    pagination_class = LargeResultsSetPagination()
    permission_classes = (IsAuthenticated,)

    def get_field_names(self, model, ignore_fields = []):
        """
        get all model field names (as strings) and returns a list of them ignoring the ones we know don't work
        """
        fields = model._meta.get_fields()
        model_field_names = list(set([f.name for f in fields if f.name not in ignore_fields]))
        return model_field_names


    def get_lookup_fields(self, model, fields=None):
        """
        compares the lookups we want vs the lookups that are available. It ignores the unavailable fields we passed
        """
        model_field_names = self.get_field_names(model)
        if fields is not None:
            """
            iterate and verify they are valid by only including the valid ones
            """
            lookup_fields = []
            for field in fields:
                if "__" in field:
                    # __ is for ForeignKey lookups
                    lookup_fields.append(field)
                elif field in model_field_names:
                    lookup_fields.append(field)
        else:
            """
            no field names passed, use default model fields
            """
            lookup_fields = model_field_names
        return lookup_fields


    def queryset_to_dataset(self, queryset, fields=None):
        """
        turn queryser it into a list of dictionaries with key/value pairs.
        """
        lookup_fields = self.get_lookup_fields(queryset.model, fields=fields)
        return list(queryset.values(*lookup_fields))


    def convert_to_csv(self, qs, fields=None):
        """
        convert to csv 
        """
        df = pd.DataFrame(qs)

        with StringIO() as b:
            df.to_csv(b, index=False, sep=",")

            response = HttpResponse(
                b.getvalue(),
                content_type='text/comma-separated-values'
            )
            filename = "results.csv"
            response['Content-Disposition'] = 'attachment; filename=%s' % (filename)
            return response


    def get(self, request, format=None, *args, **kwargs):
        """
        get query_params
        """
        keywords = self.request.query_params.get("keyword", None)
        source_types = self.request.query_params.get("source", None)
        sold_status  = self.request.query_params.get("sold", None)
        download = True if self.request.query_params.get("download", None) == 'true' else False
        minimum_price = self.request.query_params.get("price_min", None)
        maximum_price = self.request.query_params.get("price_max", None)
        minimum_date = self.request.query_params.get("date_min", None)
        maximum_date = self.request.query_params.get("date_max", None)
        sort_by = self.request.query_params.get("sort_by", None)
        order = self.request.query_params.get("order", None)
        jewelry_id = self.kwargs.get('jewelry_id')

        # raw_query = 'select * from result where id in(SELECT distinct (r.id) FROM result r LEFT JOIN run_usertask_result rutr ON rutr.result_id = r.id LEFT JOIN user_task ut ON ut.id = rutr.user_task_id )'
        raw_query = 'SELECT r.id as result_id, r.url as url_lot, ut.id as user_task_id, ut.url as url_sale,*FROM result r LEFT JOIN ( SELECT MAX(user_task_id) max_user_task_id, result_id FROM run_usertask_result GROUP BY result_id) rutr ON rutr.result_id = r.id LEFT JOIN user_task ut ON ut.id = rutr.max_user_task_id'

        # filter logic 
        queryset = Result.objects.raw(raw_query)

        if self.request.user.is_authenticated:
            user = self.request.user
            if not user.is_superuser:
                raw_query = raw_query + ' where r.is_vca=true'
                queryset= Result.objects.raw(raw_query)
        if  keywords:
            query = []
            """
            filter on keywords
            """
            for keyword in  keywords.split(','):
                query.append(
                    f"(LOWER(r.lot_title) ILIKE '%%{keyword}%%' or LOWER(r.lot_description) ILIKE '%%{keyword}%%' or LOWER(r.condition_report) ILIKE '%%{keyword}%%' or LOWER(r.pre_lot_text) ILIKE '%%{keyword}%%' or LOWER(r.artist_creator_brand) ILIKE '%%{keyword}%%' or LOWER(r.provenance) ILIKE '%%{keyword}%%' or LOWER(r.literature) ILIKE '%%{keyword}%%' or LOWER(r.special_notice) ILIKE '%%{keyword}%%' or LOWER(r.sale_room_notice) ILIKE '%%{keyword}%%' or LOWER(r.serial_number) ILIKE '%%{keyword}%%')"
                )
            raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +   ' and '.join(query)
            queryset = Result.objects.raw(raw_query)

        if  source_types:
            query = []
            """
            filter on source types
            """
            for source in  source_types.split(','):
                _str = f"regexp_replace(ut.source, '[^[:alnum:]]', '', 'g') ILike '{source}'"
                # _str =  f"ut.source ILIKE  '{source}'"
                query.append(_str)
            raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +  '( '  + ' or '.join(query) + ')'
            queryset = Result.objects.raw(raw_query)
        

        if  sold_status:                  
            """
            filter on sold status
            """
            if sold_status == '1':
                query =  "r.status in ('Sold', 'Closed', 'SOLD')"
                raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +   query
            elif sold_status == '0':
                query =  "r.status not in ('Sold', 'Closed', 'SOLD')"
                raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +   query
            else:
                pass
            queryset = Result.objects.raw(raw_query)

        if minimum_price:
            if maximum_price:
                query =  f"r.price between {minimum_price} and {maximum_price}"
                raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +   query
                queryset = Result.objects.raw(raw_query)
            else:
                query =  f"r.price >= {minimum_price}"
                raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +   query
                queryset = Result.objects.raw(raw_query)
        
        if maximum_price and not minimum_price:
            query =  f"r.price <= {maximum_price}"
            raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +   query
            queryset = Result.objects.raw(raw_query)


        if minimum_date and maximum_date:
            """
            filter on min/max date
            """
            query =  f"r.date_of_sale between cast('{minimum_date}' as date) and cast('{maximum_date}' as date)"
            raw_query = raw_query + '{}'.format(' where ' if 'where' not in raw_query else ' and ') +   query
            queryset = Result.objects.raw(raw_query)


        if sort_by:
            if sort_by == "price":
                if order == "desc":
                    query =  " order by r.price desc nulls last"
                    raw_query = raw_query + query
                    queryset = Result.objects.raw(raw_query)
                else:
                    query =  " order by r.price asc nulls last"
                    raw_query = raw_query + query
                    queryset = Result.objects.raw(raw_query)
                    
            elif sort_by == "date":
                if order == "desc":
                    query =  " order by r.date_of_sale desc nulls last"
                    raw_query = raw_query + query
                    queryset = Result.objects.raw(raw_query)
                else:
                    query =  " order by r.date_of_sale asc nulls last"
                    raw_query = raw_query + query
                    queryset = Result.objects.raw(raw_query)
            else:
                pass

        
        if jewelry_id:
            raw_query = f'SELECT r.id as result_id, r.url as url_lot, ut.id as user_task_id, ut.url as url_sale,*FROM result r LEFT JOIN ( SELECT MAX(user_task_id) max_user_task_id, result_id FROM run_usertask_result GROUP BY result_id) rutr ON rutr.result_id = r.id LEFT JOIN user_task ut ON ut.id = rutr.max_user_task_id WHERE r.id = {jewelry_id}'
            queryset = Result.objects.raw(raw_query)

        if download:
            """
            download queryset as csv
            """
            serializer = ResultsSerializer(queryset, many=True)
            return self.convert_to_csv(serializer.data)

        page = self.pagination_class.paginate_queryset(queryset=queryset, request=request)
        if page is not None:
            serializer = ResultsSerializer(page, many=True)
            return self.pagination_class.get_paginated_response(serializer.data)

        serializer = ResultsSerializer(queryset, many=True)
        return Response(serializer.data)

